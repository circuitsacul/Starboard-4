use async_trait::async_trait;
use lazy_static::lazy_static;
use twilight_model::{
    channel::{message::embed::Embed, Attachment as ReceivedAttachment},
    http::attachment::Attachment,
};
use twilight_util::builder::embed::{EmbedBuilder, ImageSource};

use crate::{
    client::bot::StarboardBot,
    constants,
    errors::{StarboardError, StarboardResult},
};

pub struct AttachmentHandle {
    pub filename: String,
    pub content_type: Option<String>,
    pub url: String,
}

impl AttachmentHandle {
    pub async fn as_attachment(
        &self,
        bot: &StarboardBot,
        id: u64,
    ) -> StarboardResult<Option<Attachment>> {
        // this should always be a proxy url, but we do this to make 100%
        // sure that there isn't a bug that could potentially leak the VPS ip.
        {
            lazy_static! {
                static ref RE: regex::Regex = regex::Regex::new(
                    r#"^https://[\w\.\-]*\.(discord\.com|discordapp\.com|discordapp.net)"#
                )
                .unwrap();
            }

            if !RE.is_match(&self.url) {
                return Ok(None);
            }
        }

        // we only want to download files under 8mb
        let head = bot.reqwest.head(&self.url).send().await?;
        let bytes = &head.headers()["content-length"];
        let bytes = bytes.to_str().unwrap().parse::<i64>().unwrap();

        if bytes > 8_000_000 {
            return Ok(None);
        }

        // download the file
        let file = bot.reqwest.get(&self.url).send().await?.bytes().await?;

        Ok(Some(Attachment::from_bytes(
            self.filename.clone(),
            file.to_vec(),
            id,
        )))
    }

    pub fn from_attachment(attachment: &ReceivedAttachment) -> Self {
        Self {
            filename: attachment.filename.clone(),
            content_type: attachment.content_type.clone(),
            url: attachment.url.clone(),
        }
    }

    pub fn as_embed(&self) -> Option<Embed> {
        self.embedable_image().map(|image| {
            EmbedBuilder::new()
                .image(image)
                .color(constants::EMBED_DARK_BG)
                .build()
        })
    }

    pub fn url_list_item(&self) -> String {
        format!("[{}]({})", self.filename, self.url)
    }

    pub fn embedable_image(&self) -> Option<ImageSource> {
        if self.filename.starts_with("SPOILER_") {
            return None;
        }

        if let Some(ct) = &self.content_type {
            if ct.starts_with("image") {
                return Some(ImageSource::url(&self.url).unwrap());
            }
        }

        None
    }
}

#[async_trait]
pub trait VecAttachments {
    async fn as_attachments(&self, bot: &StarboardBot) -> (Vec<Attachment>, Vec<StarboardError>);
}

#[async_trait]
impl VecAttachments for Vec<AttachmentHandle> {
    async fn as_attachments(&self, bot: &StarboardBot) -> (Vec<Attachment>, Vec<StarboardError>) {
        let mut attachments = Vec::new();
        let mut errors = Vec::new();
        for (current_id, attachment) in self.iter().enumerate() {
            match attachment.as_attachment(bot, current_id as u64).await {
                Err(why) => errors.push(why),
                Ok(Some(file)) => attachments.push(file),
                Ok(None) => {}
            }
        }
        (attachments, errors)
    }
}
